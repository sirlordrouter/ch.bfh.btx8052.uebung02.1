import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Patient implements Runnable{

	private DocOffice medicalPractice;
	private int minTimeBetweenConsultations;
	private int maxTimeBetweenConsultations;
	private int numberOfConsultations = 0;

	public Patient(DocOffice aMedicalPractice,
			int aMinTimeBetweenConsultations, int aMaxTimeBetweenConsultations) {
		medicalPractice = aMedicalPractice;
		minTimeBetweenConsultations = aMinTimeBetweenConsultations;
		maxTimeBetweenConsultations = aMaxTimeBetweenConsultations;
	}

	@Override
	public void run() {
		System.out.println(Thread.currentThread().getName() + ": Here I am.");
		// Go to the doctor's forever.
		while (true) {
			try {
				// Try to enter doctor's office.
				numberOfConsultations = medicalPractice
						.Consultation(numberOfConsultations);
				// Wait for the next consultation.
				medicalPractice.sleepRandomTime(minTimeBetweenConsultations,
						maxTimeBetweenConsultations);
			} catch (InterruptedException exception) {
				// With this classes, no interrupt can be thrown.
			}
		}
		
	}
}
