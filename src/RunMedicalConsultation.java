import java.util.Timer;
import java.util.TimerTask;

public class RunMedicalConsultation {

	/**
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {

		// All times in seconds.
		final int NUMBER_OF_PATIENTS = 4;
		final int MIN_CONSULTATION_TIME = 1;
		final int MAX_CONSULTATION_TIME = 4;
		final int MIN_TIME_BETWEEN_CONSULTATIONS = 5;
		final int MAX_TIME_BETWEEN_CONSULTATIONS = 10;

		System.out.println("Number of Patients: " + NUMBER_OF_PATIENTS);

		System.out.println("Minimum consultation time (s): "
				+ MIN_CONSULTATION_TIME);
		System.out.println("Maximum consultation time (s): "
				+ MAX_CONSULTATION_TIME);
		System.out.println("Minimum time between consultations (s): "
				+ MIN_TIME_BETWEEN_CONSULTATIONS);
		System.out.println("Maximum time between consultations (s): "
				+ MAX_TIME_BETWEEN_CONSULTATIONS + "\n");

		final Thread[] threadReference = new Thread[NUMBER_OF_PATIENTS];

		DocOffice docOffice = new DocOffice(MIN_CONSULTATION_TIME,
				MAX_CONSULTATION_TIME);

		for (int i = 0; i < threadReference.length; i++) {
			String threadName = "P" + (i + 1);
			threadReference[i] = new Thread((new Patient(docOffice,
					MIN_TIME_BETWEEN_CONSULTATIONS,
					MAX_TIME_BETWEEN_CONSULTATIONS)), threadName);
			threadReference[i].start();
		}

		// Timer timer = new Timer();
		// timer.schedule(new TimerTask() {
		//
		// @Override
		// public void run() {
		// for (int i = 0; i < threadReference.length; i++) {
		// try {
		// threadReference[i].join();
		//
		// } catch (InterruptedException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// }
		// System.out.println("All threads were interrupted.");
		// this.cancel();
		// }}, 30000000);
		//
		// System.out.println("All threads have terminated");
	}

}
