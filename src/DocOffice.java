import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DocOffice {

	private int consultations = 0;
	private int minConsultationTime;
	private int maxConsultationTime;
	private Random randomGenerator = new Random();
	private Lock doctor = new ReentrantLock(true);

	public DocOffice(int aMinConsultationTime, int aMaxConsultationTime) {
		minConsultationTime = aMinConsultationTime;
		maxConsultationTime = aMaxConsultationTime;
	}

	public int Consultation(int numberOfConsultations) 
			throws InterruptedException {
		doctor.lock();
		try {
			consultations++;
			numberOfConsultations++;
			System.out.println(Thread.currentThread().getName()
					+ ": Entering doctor's office (my " + numberOfConsultations
					+ "th consultation of " + consultations + " consult.).");
			sleepRandomTime(minConsultationTime, maxConsultationTime);
			System.out.println(" " + Thread.currentThread().getName()
					+ ": Leaving doctor's office.");
			return numberOfConsultations;
			
		} finally {
			doctor.unlock();
		}

	}
	
	public void sleepRandomTime(int minValue, int maxValue)
			throws InterruptedException {
		int timeToSleep = randomGenerator.nextInt((maxValue - minValue) * 1000);
		Thread.sleep(timeToSleep + minValue * 1000);
	}
}
