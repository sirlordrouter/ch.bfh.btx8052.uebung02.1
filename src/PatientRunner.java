import java.util.Random;

public class PatientRunner implements Runnable {

	private Patient aPatient;
	private int aDuration;
	private final int minConsultationTime = 1000;
	private final int maxConsultationTime = 4000;
	private final int minTimeBetweenConsultation = 5000;
	private final int maxTimeBetweenConsultation = 10000;

	public PatientRunner(Patient aPatient) {
		this.aPatient = aPatient;
	}

	@Override
	public void run() {

		while (!Thread.currentThread().isInterrupted()) {
			Random aRandomNumber = new Random();
			int consultationTime = minConsultationTime
					+ aRandomNumber.nextInt(maxConsultationTime
							- minConsultationTime);
			int timeBetweenConsultation = minTimeBetweenConsultation
					+ aRandomNumber.nextInt(maxTimeBetweenConsultation
							- minTimeBetweenConsultation);
			//Patient.consultationLock.lock();
			//aPatient.Consultation(consultationTime);
			try {
				Thread.sleep(timeBetweenConsultation);
			} catch (InterruptedException e) {
				//Falls Thread im Zustand Blocked ist interrupted wird. 
				Thread.currentThread().interrupt(); 
			}
		}
	

	}

}
